terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
      region = "us-east-1"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "vault" {
  address = "http://active.vault.service.consul:8200"
}