data "aws_caller_identity" "current" {}

resource "aws_iam_user" "vault_aws_user" {
  name = "vault"
}

resource "aws_iam_access_key" "vault_aws_access_key" {
  user = aws_iam_user.vault_aws_user.name
}

resource "vault_generic_secret" "vault_aws_access_key" {
  path = "kv/aws/accounts/vault/dev"

  data_json = <<EOT
{
  "access_key": "${aws_iam_access_key.vault_aws_access_key.id}",
  "secret_key": "${aws_iam_access_key.vault_aws_access_key.secret}",
  "aws_account_id": "${data.aws_caller_identity.current.account_id}"
}
EOT
}

resource "aws_iam_user_policy" "vault_assumerole_admin" {
  name = "vault-assumerole-admin"
  user = aws_iam_user.vault_aws_user.name

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
    {
        Effect = "Allow",
        Action = "sts:AssumeRole",
        Resource = aws_iam_role.administrator-vault-role.arn
    }]
  })
}

resource "aws_iam_user_policy" "vault_assumerole_route53" {
  name = "vault-assumerole-route53"
  user = aws_iam_user.vault_aws_user.name

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
    {
        Effect = "Allow",
        Action = "sts:AssumeRole",
        Resource = aws_iam_role.route53-vault-role.arn
    }]
  })
}

resource "aws_iam_role" "route53-vault-role" {
  name = "route53-vault"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
    {
      Action = "sts:AssumeRole",
      Principal = {
        AWS = aws_iam_user.vault_aws_user.arn
      },
      Effect = "Allow"
    }]
  })
}


resource "aws_iam_role" "administrator-vault-role" {
  name = "administrator-vault"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
    {
      Action = "sts:AssumeRole",
      Principal = {
        AWS = aws_iam_user.vault_aws_user.arn
      },
      Effect = "Allow"
    }]
  })
}

data "aws_iam_policy" "administrator_access" {
  arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

data "aws_iam_policy" "route53_access" {
  arn = "arn:aws:iam::aws:policy/AmazonRoute53FullAccess"
}

resource "aws_iam_role_policy_attachment" "admin-attach" {
  role       = aws_iam_role.route53-vault-role.name
  policy_arn = data.aws_iam_policy.administrator_access.arn
}

resource "aws_iam_role_policy_attachment" "route53-attach" {
  role       = aws_iam_role.administrator-vault-role.name
  policy_arn = data.aws_iam_policy.route53_access.arn
}
