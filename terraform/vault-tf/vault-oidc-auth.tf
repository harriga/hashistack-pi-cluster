resource "vault_jwt_auth_backend" "keycloak" {
  description        = "Keycloak"
  path               = "oidc"
  type               = "oidc"
  oidc_discovery_url = var.keycloak_oidc_discovery_url
  oidc_client_id     = "dev-vault-client-oidc"
  oidc_client_secret = data.vault_generic_secret.vault_openid_client.data["client_secret"]
  default_role       = "default-role"
}

resource "vault_jwt_auth_backend_role" "default-role" {
  backend         = vault_jwt_auth_backend.keycloak.path
  role_name       = "default-role"
  token_policies  = ["default"]
  groups_claim    = "groups"
  user_claim = "email"
  role_type             = "oidc"
  allowed_redirect_uris = [
    "http://active.vault.service.consul:8200/ui/vault/auth/oidc/oidc/callback",
    "http://active.vault.service.consul:8250/oidc/callback",
    "http://localhost:8250/oidc/callback",
    "http://localhost:8200/ui/vault/auth/oidc/oidc/callback"
    ]
}


resource "vault_identity_group" "aws_admins" {
  name     = "vault-aws-admin"
  type     = "external"
  policies = ["vault-aws-admin"]
}

resource "vault_identity_group_alias" "aws_admins_alias" {
  name           = "dev-aws-admin-role"
  mount_accessor = vault_jwt_auth_backend.keycloak.accessor
  canonical_id   = vault_identity_group.aws_admins.id
}

data "template_file" "vault_aws_admin_policy_template" {
  template = file("${path.module}/policies/vault-aws-admin-policy.hcl")
}

resource "vault_policy" "vault_aws_admin_policy" {
  name = "vault-aws-admin"
  policy = data.template_file.vault_aws_admin_policy_template.rendered
}

resource "vault_identity_group" "vault_admins" {
  name     = "vault-admin"
  type     = "external"
  policies = ["vault-admin"]
}

resource "vault_identity_group_alias" "group-alias" {
  name           = "dev-vault-admin-role"
  mount_accessor = vault_jwt_auth_backend.keycloak.accessor
  canonical_id   = vault_identity_group.vault_admins.id
}

resource "vault_identity_group" "vault_ssh" {
  name     = "vault-ssh"
  type     = "external"
  policies = ["vault-ssh"]
}

resource "vault_identity_group_alias" "ssh-group-alias" {
  name           = "dev-vault-ssh-role"
  mount_accessor = vault_jwt_auth_backend.keycloak.accessor
  canonical_id   = vault_identity_group.vault_ssh.id
}

data "template_file" "vault_admin_policy_template" {
  template = file("${path.module}/policies/vault-admin-policy.hcl")
}

resource "vault_policy" "vault_admin_policy" {
  name = "vault-admin"
  policy = data.template_file.vault_admin_policy_template.rendered
}