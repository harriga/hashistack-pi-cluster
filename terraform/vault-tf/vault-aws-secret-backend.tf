
data "vault_generic_secret" "dev_aws_account_credentials" {
  path = "kv/aws/accounts/vault/dev"
}

resource "vault_aws_secret_backend" "aws" {
  access_key = data.vault_generic_secret.dev_aws_account_credentials.data["access_key"]
  secret_key = data.vault_generic_secret.dev_aws_account_credentials.data["secret_key"]
  description = "AWS secrets backend"
}

resource "vault_aws_secret_backend_role" "ec2_admin_vault" {
  backend = vault_aws_secret_backend.aws.path
  name = "ec2-admin"
  credential_type = "assumed_role"
  role_arns = [ "arn:aws:iam::${data.vault_generic_secret.dev_aws_account_credentials.data["aws_account_id"]}:role/administrator-vault" ]
  default_sts_ttl = 3600
  policy_document = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ec2:*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "vault_aws_secret_backend_role" "aws_admin_vault" {
  name = "aws-admin"
  backend = vault_aws_secret_backend.aws.path
  credential_type = "assumed_role"
  role_arns = [ "arn:aws:iam::${data.vault_generic_secret.dev_aws_account_credentials.data["aws_account_id"]}:role/administrator-vault" ]
  default_sts_ttl = 3600
}

resource "vault_aws_secret_backend_role" "route53_admin_vault" {
  name = "route53-admin"
  backend = vault_aws_secret_backend.aws.path
  credential_type = "assumed_role"
  role_arns = [ "arn:aws:iam::${data.vault_generic_secret.dev_aws_account_credentials.data["aws_account_id"]}:role/route53-vault" ]
  default_sts_ttl = 3600
}

