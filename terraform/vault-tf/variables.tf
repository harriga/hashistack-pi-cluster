variable keycloak_oidc_discovery_url {
  type = string
  default = "https://keycloak.phiji.com/auth/realms/Dev"
}