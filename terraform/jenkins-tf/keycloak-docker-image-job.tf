resource jenkins_job keycloak_arm {
  name     = "keycloak-docker-image"
  template = templatefile("${path.module}/templates/keycloak-docker-image.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "Keycloak docker/arm build"
  }
}
