resource jenkins_job jenkins_arm_master {
  name     = "jenkins-arm-master"
  template = templatefile("${path.module}/templates/jenkins-arm-master.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "Jenkins master docker/arm build"
  }
}
