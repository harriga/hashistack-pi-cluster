resource jenkins_job pact_arm {
  name     = "pact-docker-image"
  template = templatefile("${path.module}/templates/pact-docker-image.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "PACT broker docker/arm build"
  }
}
