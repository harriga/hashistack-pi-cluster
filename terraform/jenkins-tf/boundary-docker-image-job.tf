resource jenkins_job boundary_arm {
  name     = "boundary-docker-image"
  template = templatefile("${path.module}/templates/boundary-docker-image.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "Boundary arm32 docker image build"
  }
}
