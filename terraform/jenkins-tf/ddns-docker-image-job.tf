resource jenkins_job ddns_arm {
  name     = "ddns-docker-image"
  template = templatefile("${path.module}/templates/ddns-docker-image.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "Route53 ddns docker/arm build"
  }
}
