resource jenkins_job jenkins_arm_agent {
  name     = "jenkins-arm-agent"
  template = templatefile("${path.module}/templates/jenkins-arm-agent.xml.tpl", {
             hashistack_repo = var.hashistack_repo
             })

  parameters = {
    description = "Jenkins agent docker/arm build"
  }
}
