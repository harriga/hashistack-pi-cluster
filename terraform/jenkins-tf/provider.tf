provider jenkins {
  server_url = "https://jenkins.phiji.com"
}

terraform {
  required_providers {
    jenkins = {
      source  = "taiidani/jenkins"
      version = ">= 0.5.0"
    }
  }
}
