

resource "boundary_auth_method" "password" {
  name     = "Internal Password Auth"
  scope_id = boundary_scope.dev_scope.id
  type     = "password"
}

resource "random_password" "user_password" {
  for_each    = var.users
  length = 10
  special = true
}

resource "boundary_account" "users_acct" {
  for_each       = var.users
  name           = each.key
  description    = "User account for ${each.key}"
  type           = "password"
  login_name     = lower(each.key)
  password       = random_password.user_password[each.key].result
  auth_method_id = boundary_auth_method.password.id
}



resource "boundary_user" "users" {
  for_each    = var.users
  name        = each.key
  description = "User resource for ${each.key}"
  scope_id    = boundary_scope.dev_scope.id
  account_ids = [ boundary_account.users_acct[each.key].id ]
}

resource "boundary_role" "organization_admin" {
  name        = "dev_admin_role"
  description = "Administrator role"
  principal_ids = concat(
    [for user in boundary_user.users: user.id]
  )
  grant_strings   = ["id=*;type=*;actions=*"]
  grant_scope_id = boundary_scope.core_infra.id
  scope_id = boundary_scope.dev_scope.id
}

resource "boundary_scope" "core_infra" {
  name                   = "Dev Infrastructure"
  description            = "Dev Project"
  scope_id               = boundary_scope.dev_scope.id
  auto_create_admin_role = true
}

resource "boundary_host_catalog" "raspberry_pis" {
  name        = "raspberry_pis"
  description = "Raspberry Pi host catalog"
  type        = "static"
  scope_id    = boundary_scope.core_infra.id
}

resource "boundary_host" "raspberry_pis" {
  for_each        = var.raspberry_pi_ips
  type            = "static"
  name            = "raspberry_pi_${each.value}"
  description     = "Raspberry Pi"
  address         = each.key
  host_catalog_id = boundary_host_catalog.raspberry_pis.id
}

resource "boundary_host_set" "raspberry_pi" {
  type            = "static"
  name            = "raspberry_pi"
  description     = "Host set for Raspberry Pis"
  host_catalog_id = boundary_host_catalog.raspberry_pis.id
  host_ids        = [for host in boundary_host.raspberry_pis : host.id]
}

resource "boundary_host_catalog" "vault" {
  name        = "vault"
  description = "Vault host catalog"
  type        = "static"
  scope_id    = boundary_scope.core_infra.id
}

resource "boundary_host" "vault" {
  for_each        = var.vault
  type            = "static"
  name            = each.value
  description     = "HashiCorp Vault"
  address         = each.key
  host_catalog_id = boundary_host_catalog.vault.id
}

resource "boundary_host_set" "vault" {
  type            = "static"
  name            = "vault"
  description     = "Host set for Vault"
  host_catalog_id = boundary_host_catalog.vault.id
  host_ids        = [for host in boundary_host.vault : host.id]
}

resource "boundary_host_catalog" "nomad" {
  name        = "nomad"
  description = "Nomad host catalog"
  type        = "static"
  scope_id    = boundary_scope.core_infra.id
}

resource "boundary_host" "nomad" {
  for_each        = var.nomad
  type            = "static"
  name            = each.value
  description     = "HashiCorp Nomad"
  address         = each.key
  host_catalog_id = boundary_host_catalog.nomad.id
}

resource "boundary_host_set" "nomad" {
  type            = "static"
  name            = "nomad"
  description     = "Host set for Nomad"
  host_catalog_id = boundary_host_catalog.nomad.id
  host_ids        = [for host in boundary_host.nomad : host.id]
}

resource "boundary_target" "raspberry_pi_ssh" {
  type         = "tcp"
  name         = "Raspberry Pi"
  description  = "Backend SSH target"
  scope_id     = boundary_scope.core_infra.id
  default_port = "22"

  host_set_ids = [
    boundary_host_set.raspberry_pi.id
  ]
}

resource "boundary_target" "vault_api" {
  type         = "tcp"
  name         = "Vault API"
  description  = "Backend Vault target"
  scope_id     = boundary_scope.core_infra.id
  default_port = "8200"
  session_connection_limit = 2
  host_set_ids = [
    boundary_host_set.vault.id
  ]
}

resource "boundary_target" "nomad" {
  type         = "tcp"
  name         = "Nomad API"
  description  = "Backend Nomad target"
  scope_id     = boundary_scope.core_infra.id
  default_port = "4646"
  session_connection_limit = 2
  host_set_ids = [
    boundary_host_set.nomad.id
  ]
}