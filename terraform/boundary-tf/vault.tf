provider "vault" {
  address = "http://active.vault.service.consul:8200"
}

resource "vault_generic_secret" "boundary_account_secrets" {
  for_each       = var.users
  path = join("/", ["kv/boundary/accounts", lower(each.key)])
  data_json = <<EOT
{
  "password":   "${random_password.user_password[each.key].result}"
}
EOT
}