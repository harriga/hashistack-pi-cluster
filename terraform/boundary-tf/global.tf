resource "boundary_scope" "global_dev_scope" {
  global_scope = true
  description  = "Global Scope"
  scope_id     = "global"
  name         = "global"
}

resource "boundary_scope" "dev_scope" {
  name                     = "Dev Scope"
  description              = "Scope for internal dev network"
  scope_id                 = boundary_scope.global_dev_scope.id
  auto_create_admin_role   = true
  auto_create_default_role = true
}