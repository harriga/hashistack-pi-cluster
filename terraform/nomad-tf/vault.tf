provider "vault" {
  address = "http://active.vault.service.consul:8200"
}

data "vault_generic_secret" "sickbeard_openid_client" {
  path = "kv/keycloak/realms/dev/sickbeard"
}

data "vault_generic_secret" "sickgear_openid_client" {
  path = "kv/keycloak/realms/dev/sickgear"
}

data "vault_generic_secret" "jenkins_openid_client" {
  path = "kv/keycloak/realms/dev/jenkins"
}

data "vault_generic_secret" "homebridge_openid_client" {
  path = "kv/keycloak/realms/dev/homebridge"
}

data "vault_generic_secret" "sabnzb_openid_client" {
  path = "kv/keycloak/realms/dev/sabnzb"
}

data "vault_generic_secret" "traefik_openid_client" {
  path = "kv/keycloak/realms/dev/traefik"
}

data "vault_generic_secret" "grafana_openid_client" {
  path = "kv/keycloak/realms/dev/grafana"
}

data "vault_generic_secret" "transmission_openid_client" {
  path = "kv/keycloak/realms/dev/transmission"
}

data "vault_generic_secret" "couchpotato_openid_client" {
  path = "kv/keycloak/realms/dev/couchpotato"
}

data "vault_generic_secret" "jenkins_secrets" {
  path = "kv/traefik/routes/jenkins"
}

data "vault_generic_secret" "keycloak_secrets" {
  path = "kv/traefik/routes/keycloak"
}

data "vault_generic_secret" "keycloak_db_secrets" {
  path = "kv/keycloak/db"
}

data "vault_generic_secret" "keycloak_app_secrets" {
  path = "kv/keycloak/keycloak"
}

data "vault_generic_secret" "traefik_oauth_proxy_secrets" {
  path = "kv/traefik/oauth-proxy"
}

data "vault_generic_secret" "sickbeard_oauth_proxy_secrets" {
  path = "kv/sickbeard/oauth-proxy"
}

data "vault_generic_secret" "sickgear_oauth_proxy_secrets" {
  path = "kv/sickgear/oauth-proxy"
}

data "vault_generic_secret" "sab_oauth_proxy_secrets" {
  path = "kv/sabnzb/oauth-proxy"
}

data "vault_generic_secret" "couchpotato_oauth_proxy_secrets" {
  path = "kv/couchpotato/oauth-proxy"
}

data "vault_generic_secret" "homebridge_oauth_proxy_secrets" {
  path = "kv/homebridge/oauth-proxy"
}

data "vault_generic_secret" "transmission_oauth_proxy_secrets" {
  path = "kv/transmission/oauth-proxy"
}

data "vault_generic_secret" "grafana_config_secrets" {
  path = "kv/grafana/config"
}

data "vault_generic_secret" "pact_secrets" {
  path = "kv/pact/config"
}

data "vault_generic_secret" "nomad_ca" {
  path = "kv/vault_root_ca"
}

data "vault_generic_secret" "boundary_secrets" {
  path = "kv/boundary/config"
}

resource "vault_mount" "nomad-mount" {
  path        = "nomad"
  type        = "nomad"
  description = "Nomad secrets mount"
}

resource "vault_generic_endpoint" "nomad_config" {
  path = "nomad/config/access"

  data_json = jsonencode({
      address = "https://nomad.service.consul:4646",
      token = nomad_acl_token.vault-integration-token.secret_id
      ca_cert = base64decode(data.vault_generic_secret.nomad_ca.data["ca"])
    })

}

resource "vault_generic_endpoint" "nomad_developer_vault_role" {
  path = "nomad/role/developer"
  data_json = jsonencode({
    policies=["developer"]
  })
}

resource "vault_generic_endpoint" "nomad_operator_vault_role" {
  path = "nomad/role/operations"
  data_json = jsonencode({
    policies=["operations"]
  })
}
