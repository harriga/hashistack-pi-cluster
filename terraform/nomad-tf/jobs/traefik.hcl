job "traefik-service" {
  region      = "global"
  datacenters = ["DC0"]
  type        = "service"

  group "traefik" {

    vault {
      policies = ["nomad-traefik-policy"]
    }

    volume "acme_volume" {
      type = "host"
      source = "acme_volume"
      read_only = false
    }

    task "traefik" {

      constraint {
        attribute = "${attr.unique.network.ip-address}"
        value     = "192.168.1.203"
      }

      driver = "docker"
      volume_mount {
        volume      = "acme_volume"
        destination = "/data"
      }
      config {
        image        = "traefik:v2.2"
        network_mode = "host"

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
        ]
      }

      template {
        data = <<EOH
AWS_ACCESS_KEY_ID = "{{ with secret "kv/data/traefik/aws" }}{{.Data.data.aws_access_key_id}}{{end}}"
AWS_SECRET_ACCESS_KEY = "{{ with secret "kv/data/traefik/aws" }}{{.Data.data.aws_secret_access_key}}{{end}}"
AWS_HOSTED_ZONE_ID = "{{ with secret "kv/data/traefik/aws" }}{{.Data.data.aws_hosted_zone_id}}{{end}}"
AWS_REGION = "{{ with secret "kv/data/traefik/aws" }}{{.Data.data.aws_region}}{{end}}"
EOH
        destination = "secrets/file.env"
        env         = true
      }

      template {
        data = <<EOF
[entryPoints]
    [entryPoints.http]
    address = ":80"
    [entryPoints.https]
    address = ":443"
    [entryPoints.boundary-worker]
    address = ":4443"
    [entryPoints.traefik]
    address = ":81"

[api]
    dashboard = true
    insecure = true

[log]
  level = "DEBUG"

[certificatesResolvers.{{ with secret "kv/data/traefik/acme" }}{{.Data.data.resolver_name}}{{end}}.acme]
  email = "{{ with secret "kv/data/traefik/acme" }}{{.Data.data.resolver_email}}{{end}}"
  storage = "/data/acme.json"

  [certificatesResolvers.{{ with secret "kv/data/traefik/acme" }}{{.Data.data.resolver_name}}{{end}}.acme.dnsChallenge]
    provider = "route53"
    delayBeforeCheck = 0
    
[serversTransport]
  insecureSkipVerify = true

# Enable Consul Catalog configuration backend.
[providers.consulCatalog]
    prefix           = "traefik"
    exposedByDefault = false

    [providers.consulCatalog.endpoint]
      address = "127.0.0.1:8500"
      scheme  = "http"

[providers.file]
    filename = "local/proxy-config.toml"
EOF
        destination = "local/traefik.toml"
      }

      template {
        data = <<EOF
[http.routers.boundary-router]
  rule = "Host(`boundary.{{ with secret "kv/data/traefik/acme" }}{{.Data.data.resolver_name}}{{end}}.com`)"
  service = "boundary-loadbalancer"
  entryPoints = ["https"]
[http.routers.boundary-router.tls]
          certResolver = "{{ with secret "kv/data/traefik/acme" }}{{.Data.data.resolver_name}}{{end}}"
[[http.services.boundary-loadbalancer.loadBalancer.servers]]
  url = "https://boundary.service.consul:9200"
[tcp.routers.boundary-worker-router]
  rule = "HostSNI(`*`)"
  service = "boundary-worker-loadbalancer"
  entryPoints = ["boundary-worker"]
[tcp.routers.boundary-worker-router.tls]
  passthrough = true
[[tcp.services.boundary-worker-loadbalancer.loadBalancer.servers]]
  address = "boundary-worker.service.consul:9202"
EOF

        destination = "local/proxy-config.toml"
      }

      resources {
        cpu    = 20
        memory = 10

        network {
          mbits = 10

          port "http" {
            static = 80
          }
          port "https" {
            static = 443
          }
          port "api" {
            static = 81
          }
        }
      }

      service {
        name = "traefik"

        check {
          name     = "alive"
          type     = "tcp"
          port     = "http"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
