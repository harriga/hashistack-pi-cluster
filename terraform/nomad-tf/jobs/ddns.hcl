job "ddns-system" {
  region      = "global"
  datacenters = ["DC0"]
  type        = "batch"

  constraint {
    attribute = "${attr.unique.network.ip-address}"
    operator  = "!="
    value     = "192.168.1.203"
  }
  
  periodic {
    cron = "*/15 * * * * *"
    prohibit_overlap = true
  }

  vault { 
    policies = ["nomad-ddns-policy"]
  }

  group "ddns" {

    task "ddns" {
      driver = "docker"
      config {
        image        = "glharris/r53-ddns-armhf"
        volumes = [
          ".aws_credentials:/root/.aws/credentials",
        ]
      }

      template {
        data = <<EOH
RESOURCE_RECORD = "{{ with secret "kv/data/ddns/aws" }}{{.Data.data.resource_record}}{{end}}"
AWS_HOSTED_ZONE_ID = "{{ with secret "kv/data/ddns/aws" }}{{.Data.data.aws_hosted_zone_id}}{{end}}"
RESOURCE_TTL = "30"
RESOURCE_TYPE = "A"
EOH
        destination = "secrets/file.env"
        env         = true
      }

      template {
        data = <<EOF
[default]
aws_access_key_id={{ with secret "kv/data/ddns/aws" }}{{.Data.data.aws_access_key_id}}{{end}}
aws_secret_access_key={{ with secret "kv/data/ddns/aws" }}{{.Data.data.aws_secret_access_key}}{{end}}
EOF
        destination = ".aws_credentials"
      }

      resources {
        cpu    = 100
        memory = 64

      }
    }
  }
}
