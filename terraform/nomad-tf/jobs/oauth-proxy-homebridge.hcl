job "oauth-proxy-homebridge" {
  region      = "global"
  datacenters = ["DC0"]
  type        = "service"

  constraint {
    attribute = "$${attr.unique.network.ip-address}"
    operator  = "!="
    value     = "192.168.1.203"
  }

  group "oauth-proxy-homebridge" {
    count = 1

    task "oauth-proxy-homebridge" {
      driver = "docker"
      env {
      }
      config {
        image        = "quay.io/oauth2-proxy/oauth2-proxy:latest-armv6"
        args = [
         "--provider", "keycloak",
          "--client-id", "dev-homebridge-client-oidc",
          "--client-secret", "${client_secret}",
          "--login-url", "${login_url}",
          "--redeem-url", "${redeem_url}",
          "--validate-url", "${validate_url}",
          "--upstream", "${upstream}",
          "--email-domain", "*",
          "--cookie-secret", "${cookie_secret}",
          "--cookie-secure", "true",
          "--http-address", "0.0.0.0:4184",
          "--reverse-proxy", "true",
          "--scope", "openid",
          "--force-https", "true",
          "--keycloak-group", "dev-homebridge-admin-role",
          "--set-authorization-header", "false",
          "--set-xauthrequest", "false",
          "--pass-access-token", "false",
          "--pass-authorization-header", "false",
          "--skip-provider-button", "true"
        ]
        port_map = {
          http = 4184
        }
      }

      resources {
        cpu    = 100
        memory = 128
        network {
          port "https" {
            static = 4184
          }
        }
      }

      service {
        port = "https"
        name = "oauth-proxy-homebridge"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.oauth-proxy-homebridge.rule=Host(`${host}`)",
          "traefik.http.routers.oauth-proxy-homebridge.entryPoints=https",
          "traefik.http.routers.oauth-proxy-homebridge.tls.certresolver=${cert_resolver}",
        ]
      }
    }
  }
}
