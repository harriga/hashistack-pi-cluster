job "oauth-proxy-couchpotato" {
  region      = "global"
  datacenters = ["DC0"]
  type        = "service"

  constraint {
    attribute = "$${attr.unique.network.ip-address}"
    operator  = "!="
    value     = "192.168.1.203"
  }

  group "oauth-proxy-couchpotato" {
    count = 1

    task "oauth-proxy-couchpotato" {
      driver = "docker"
      env {
      }
      config {
        image        = "quay.io/oauth2-proxy/oauth2-proxy:latest-armv6"
        args = [
          "--provider", "keycloak",
          "--client-id", "dev-couchpotato-client-oidc",
          "--client-secret", "${client_secret}",
          "--login-url", "${login_url}",
          "--redeem-url", "${redeem_url}",
          "--validate-url", "${validate_url}",
          "--upstream", "${upstream}",
          "--email-domain", "*",
          "--cookie-secret", "${cookie_secret}",
          "--cookie-secure", "true",
          "--http-address", "0.0.0.0:4182",
          "--reverse-proxy", "true",
          "--scope", "openid",
          "--force-https", "true",
          "--keycloak-group", "dev-couchpotato-admin-role",
          "--set-authorization-header", "true",
          "--set-xauthrequest", "true",
          "--pass-access-token", "true",
          "--pass-authorization-header", "true",
          "--skip-provider-button", "true"
        ]
        port_map = {
          http = 4182
        }
      }

      resources {
        cpu    = 100
        memory = 128
        network {
          port "https" {
            static = 4182
          }
        }
      }

      service {
        port = "https"
        name = "oauth-proxy-couchpotato"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.oauth-proxy-couchpotato.entryPoints=https",
          "traefik.http.routers.oauth-proxy-couchpotato.rule=Host(`${host}`)",
          "traefik.http.routers.oauth-proxy-couchpotato.tls.certresolver=${cert_resolver}",
        ]
      }
    }
  }
}
