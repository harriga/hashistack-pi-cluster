job "nats" {
  datacenters = ["DC0"]

  vault {
    policies = ["nomad-nats-policy"]
  }

  constraint {
    attribute = "${attr.unique.network.ip-address}"
    operator  = "!="
    value     = "192.168.1.203"
  }

  group "nats-1" {

    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "nats-1" {
      driver = "docker"
      
      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-1.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.issuing_ca }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/nats-ca.pem"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-1.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.certificate }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/nats-server-cert.pem"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-1.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1"}}{{ .Data.private_key }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/nats-server-key.pem"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
tls: {
    verify_and_map: true
}
accounts: {
    TestAccount: {
        users: [
            {user: "test-client@nats"}
        ]
    }
}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/accounts.conf"
        change_mode   = "restart"
      }

      config {
        image = "nats:latest"

        args = [
          "-m", "8222",
          "-p", "4222",
          "--client_advertise", "nats-1.service.consul:4222",
          "--tls", 
          "--tlscert", "${NOMAD_SECRETS_DIR}/nats-server-cert.pem",
          "--tlskey", "${NOMAD_SECRETS_DIR}/nats-server-key.pem",
          "--tlscacert", "${NOMAD_SECRETS_DIR}/nats-ca.pem",
          "-c", "${NOMAD_SECRETS_DIR}/accounts.conf",
          "--cluster", "nats://0.0.0.0:6222"

        ]
        port_map {
          client = 4222,
          monitoring = 8222
          routing = 6222
        }
      }

      resources {
        cpu    = 50 
        memory = 50 

        network {
          mbits = 1

          port "client" {
            static = 4222
          }

          port "monitoring" {
            static = 8222
          }

          port "routing" {
            static = 6222
          }
        }
      }

      service {
        port = "client"
        name = "nats-1"

        check {
           type     = "http"
           port     = "monitoring"
           path     = "/connz"
           interval = "5s"
           timeout  = "2s"
        }
      }
    }
  }
  group "nats-2" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "nats-2" {
      driver = "docker"
      
      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-2.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.issuing_ca }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-ca.pem"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-2.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.certificate }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-server-cert.pem"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-2.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1"}}{{ .Data.private_key }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-server-key.pem"
      }

      template {
        data = <<EOH
tls: {
    verify_and_map: true
}
accounts: {
    TestAccount: {
        users: [
            {user: "test-client@nats"}
        ]
    }
}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/accounts.conf"
        change_mode   = "restart"
      }

      config {
        image = "nats:latest"

        args = [
          "-m", "8222",
          "-p", "4223",
          "--cluster", "nats://0.0.0.0:6222",
          "--client_advertise", "nats-2.service.consul:4223",
          "--tls", 
          "--tlscert", "${NOMAD_SECRETS_DIR}/nats-server-cert.pem",
          "--tlskey", "${NOMAD_SECRETS_DIR}/nats-server-key.pem",
          "--tlscacert", "${NOMAD_SECRETS_DIR}/nats-ca.pem",
          "-c", "${NOMAD_SECRETS_DIR}/accounts.conf",
          "--tlsverify",
          "--routes", "nats://ruser:T0pS3cr3t@nats-1.service.consul:6222",
        ]

        port_map {
          client = 4223,
          monitoring = 8222
          routing = 6222
        }
      }

      resources {
        cpu    = 50 
        memory = 50

        network {
          mbits = 1

          port "client" {
            static = 4223
          }

          port "monitoring" {
            static = 8222
          }

          port "routing" {
            static = 6222
          }
        }
      }
      service {
        port = "client"
        name = "nats-2"

        check {
           type     = "http"
           port     = "monitoring"
           path     = "/connz"
           interval = "5s"
           timeout  = "2s"
        }
      }
    }
  }
  group "nats-3" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    task "nats-3" {
      driver = "docker"
      
      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-3.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.issuing_ca }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-ca.pem"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-3.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.certificate }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-server-cert.pem"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/nats.consul" "common_name=nats-3.service.consul" "alt_names=localhost,nats.service.consul" "ip_sans=127.0.0.1"}}{{ .Data.private_key }}{{ end }}
EOH
        destination = "${NOMAD_SECRETS_DIR}/nats-server-key.pem"
      }

      template {
        data = <<EOH
tls: {
    verify_and_map: true
}
accounts: {
    TestAccount: {
        users: [
            {user: "test-client@nats"}
        ]
    }
}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/accounts.conf"
        change_mode   = "restart"
      }

      config {
        image = "nats:latest"

        args = [
          "-m", "8222",
          "-p", "4224",
          "--cluster", "nats://0.0.0.0:6222",
          "--client_advertise", "nats-3.service.consul:4224",
          "--tls", 
          "--tlscert", "${NOMAD_SECRETS_DIR}/nats-server-cert.pem",
          "--tlskey", "${NOMAD_SECRETS_DIR}/nats-server-key.pem",
          "--tlscacert", "${NOMAD_SECRETS_DIR}/nats-ca.pem",
          "-c", "${NOMAD_SECRETS_DIR}/accounts.conf",
          "--tlsverify",
          "--routes", "nats://ruser:T0pS3cr3t@nats-1.service.consul:6222"
        ]

        port_map {
          client = 4224,
          monitoring = 8222
          routing = 6222
        }
      }

      resources {
        cpu    = 50 
        memory = 50

        network {
          mbits = 1

          port "client" {
            static = 4224
          }

          port "monitoring" {
            static = 8222
          }

          port "routing" {
            static = 6222
          }
        }
      }
      service {
        port = "client"
        name = "nats-3"

        check {
           type     = "http"
           port     = "monitoring"
           path     = "/connz"
           interval = "5s"
           timeout  = "2s"
        }
      }
    }
  }
}