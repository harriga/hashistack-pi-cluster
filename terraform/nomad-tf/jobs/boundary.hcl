job "boundary" {
  vault {
    policies = ["nomad-boundary"]
  }

  region      = "global"
  datacenters = ["DC0"]
  type        = "service"

  group "boundary-controller" {
    count = 1

    task "boundary-controller" {
      driver = "docker"

      constraint {
        attribute = "${attr.unique.network.ip-address}"
        value     = "192.168.1.203"
      }

      config {
        image        = "glharris/boundary-arm:latest"
        args = [
          "server",
          "-config", "${NOMAD_SECRETS_DIR}/boundary.hcl"
        ]
        port_map = {
          api = 9200
          controller = 9201
        }
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.issuing_ca }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.ca"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.certificate }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.cert"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.private_key }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.key"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
disable_mlock = true

controller {
  name = "controller"
  description = "A controller"
  public_cluster_addr = "boundary.service.consul"
  database {
    url = "postgresql://boundary:{{with secret "kv/data/boundary/config"}}{{.Data.data.boundary_db_pass}}{{end}}@postgres.service.consul:5432/boundary?sslmode=disable"
  }
}

listener "tcp" {
  address = "0.0.0.0"
  purpose = "api"
  tls_disable   = false
  tls_cert_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.cert" 
  tls_key_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.key" 
  tls_ca_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.ca" 
}

listener "tcp" {
  address = "0.0.0.0"
  purpose = "cluster"
  tls_disable   = false
  tls_cert_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.cert" 
  tls_key_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.key" 
  tls_ca_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.ca" 
}

kms "transit" {
  purpose            = "root"
  address            = "http://active.vault.service.consul:8200"
  token              = "{{ env "VAULT_TOKEN" }}"
  disable_renewal    = "false"

  // Key configuration
  key_name           = "boundary-global-root"
  mount_path         = "transit/"
}

# This key is the same key used in the worker configuration
kms "transit" {
  purpose            = "worker-auth"
  address            = "http://active.vault.service.consul:8200"
  token              = "{{ env "VAULT_TOKEN" }}"
  disable_renewal    = "false"

  // Key configuration
  key_name           = "boundary-worker-auth"
  mount_path         = "transit/"
}

kms "transit" {
  purpose            = "recovery"
  address            = "http://active.vault.service.consul:8200"
  token              = "{{ env "VAULT_TOKEN" }}"
  disable_renewal    = "false"

  // Key configuration
  key_name           = "boundary-recovery"
  mount_path         = "transit/"
}
EOH
        destination = "${NOMAD_SECRETS_DIR}/boundary.hcl"
        change_mode         = "restart"
      }


      resources {
        cpu    = 50
        memory = 100

        network {
          mbits = 10
          port "controller" { static = 9201 }
          port "api" { static = 9200 }
        }
      }

      service {
        name = "boundary"
        port = "api"
      }
    }
  }

  group "boundary-worker" {

    count = 1

    task "boundary-worker" {

      constraint {
        attribute = "${attr.unique.network.ip-address}"
        value     = "192.168.1.203"
      }

      driver = "docker"

      config {
        image        = "glharris/boundary-arm:latest"
        args = [
          "server",
          "-config", "${NOMAD_SECRETS_DIR}/boundary.hcl"
        ]
        port_map = {
          worker = 9202
        }
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary-worker.phiji.com,boundary-worker.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.issuing_ca }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.ca"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary-worker.phiji.com,boundary-worker.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.certificate }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.cert"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
{{ with secret "pki/issue/boundary.consul" "common_name=boundary.consul" "alt_names=localhost,boundary.phiji.com,boundary-worker.phiji.com,boundary-worker.service.consul" "ip_sans=127.0.0.1" }}{{ .Data.private_key }}{{ end }}
EOH
        destination   = "${NOMAD_SECRETS_DIR}/tls.key"
        change_mode   = "restart"
      }

      template {
        data = <<EOH
disable_mlock = true

worker {
  name = "worker"
  description = "A default worker"
  controllers = [
    "boundary.service.consul",
  ]
  address = "0.0.0.0"
  public_addr = "boundary-worker.phiji.com:4443"
}

listener "tcp" {
  address       = "0.0.0.0"
  purpose       = "proxy"
  tls_disable   = false
  tls_cert_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.cert" 
  tls_key_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.key" 
  tls_ca_file = "{{env "NOMAD_SECRETS_DIR"}}/tls.ca" 
}

kms "transit" {
  purpose            = "worker-auth"
  address            = "http://active.vault.service.consul:8200"
  token              = "{{ env "VAULT_TOKEN" }}"
  disable_renewal    = "false"

  // Key configuration
  key_name           = "boundary-worker-auth"
  mount_path         = "transit/"
}
EOH
        destination = "${NOMAD_SECRETS_DIR}/boundary.hcl"
        change_mode         = "restart"
      }


      resources {
        cpu    = 50
        memory = 100

        network {
          mbits = 10
          port "worker" { static = 9202 }
        }
      }

      service {
        name = "boundary-worker"
        port = "worker"
      }
    }
  }

  group "postgres" {

    count = 1

    volume "boundary_postgres_volume" {
      type = "host"
      source = "boundary_postgres_volume"
      read_only = false
    }


    task "postgres" {
      driver = "docker"

      volume_mount {
        volume      = "boundary_postgres_volume"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      template {
        data = <<EOH
POSTGRES_DB = "boundary"
POSTGRES_USER = "boundary"
POSTGRES_PASSWORD = "{{with secret "kv/data/boundary/config"}}{{.Data.data.boundary_db_pass}}{{end}}"
EOH
        destination = "secrets/file.env"
        env         = true
      }

      config {
        image = "arm32v7/postgres:latest"
        port_map = {
          http = 5432
        }
      }
      service {
        name = "postgres"
        tags = ["db", "postgres"]
        port = "http"
      }

      resources {
        cpu    = 20
        memory = 50
        network {
          mbits = 10
          port "http" { static = 5432 }
        }
      }
    }
  }
}