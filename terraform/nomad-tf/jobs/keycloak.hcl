job "keycloak" {
  region      = "global"
  datacenters = ["DC0"]
  type        = "service"

  group "keycloak" {

    count = 1

    task "keycloak" {
      driver = "docker"

      config {
        image        = "glharris/keycloak-armhf:latest"
        port_map = {
          http = 8080
        }

      }

      env = {
        "KEYCLOAK_USER"= "${admin_user}"
        "KEYCLOAK_PASSWORD"= "${admin_pass}"
        "DB_VENDOR" = "mysql"
        "DB_USER" = "${db_user}"
        "DB_PASSWORD" = "${db_pass}"
        "DB_ADDR" = "mysql.service.consul"
        "KEYCLOAK_STATISTICS" = "all"
        "PROXY_ADDRESS_FORWARDING" = "true"
      }

      resources {
        cpu    = 200
        memory = 150

        network {
          mbits = 10
          port "http" {}
        }
      }

      service {
        name = "keycloak"
        port = "http"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.keycloak.rule=Host(`${host}`)",
          "traefik.http.routers.keycloak.entryPoints=https",
          "traefik.http.routers.keycloak.tls.certresolver=${cert_resolver}"
        ]
      }
    }
  }

  group "mysql" {
    count = 1 

    volume "keycloak_mysql_volume" {
      type = "host"
      source = "keycloak_mysql_volume"
      read_only = false
    }


    task "mysql" {
      driver = "docker"

      volume_mount {
        volume      = "keycloak_mysql_volume"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      env = {
        "MYSQL_ROOT_PASSWORD" = "${db_pass}"
      }

      config {
        image = "hypriot/rpi-mysql:latest"
        port_map = {
          http = 3306
        }
      }
      service {
        name = "mysql"
        tags = ["db", "mysql"]
        port = "http"
      }

      resources {
        cpu    = 100
        memory = 128
        network {
          mbits = 10
          port "http" { static = 3306 }
        }
      }
    }
  }
}
