job "jenkins" {

  constraint {
    attribute = "$${attr.unique.network.ip-address}"
    operator  = "!="
    value     = "192.168.1.203"
  }

  datacenters = ["DC0"]
  type="service"
  group "jenkins-group" {
    volume "jenkins_volume" {
      type = "host"
      source = "jenkins_volume"
      read_only = false
    }
    task "jenkins-task" {
      driver = "docker"
      service {
        port = "http"
        name = "jenkins"
        tags = [
          "traefik.enable=true",
          "traefik.http.routers.jenkins.rule=Host(`${host}`)",
          "traefik.http.routers.jenkins.entryPoints=https",
          "traefik.http.routers.jenkins.tls.certresolver=${cert_resolver}"
        ]
      }
      service {
        port = "jnlp"
        name = "jenkins-jnlp"
      }
      resources {
        cpu    = 2000
        memory = 512
        network {
          port "http" { static = 8082 }
          port "jnlp" { static = 50000 }
          mbits = 10
        }
      }
      volume_mount {
        volume      = "jenkins_volume"
        destination = "/var/jenkins_home"
      }
      config {
        image = "glharris/rpi-jenkins:latest"
        port_map = {
          http = 8080
          jenkins_jnlp = 50000
        }
      }
    }
  }
}

