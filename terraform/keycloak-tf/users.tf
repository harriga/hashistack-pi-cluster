resource "keycloak_user" "dev_user" {
  for_each = var.dev_users
  realm_id   = keycloak_realm.dev_realm.id
  username   = each.key
  enabled    = true
  email      = each.value
  email_verified = true
}

resource "keycloak_group_memberships" "sickbeard_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_sickbeard_group.id

    members  = var.dev_sickbeard_users
}

resource "keycloak_group_memberships" "couchpotato_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_couchpotato_group.id

    members  = var.dev_couchpotato_users
}

resource "keycloak_group_memberships" "grafana_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_grafana_group.id

    members  = var.dev_grafana_users
}

resource "keycloak_group_memberships" "homebridge_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_homebridge_group.id

    members  = var.dev_homebridge_users
}


resource "keycloak_group_memberships" "sabnzb_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_sabnzb_group.id

    members  = var.dev_sabnzb_users
}

resource "keycloak_group_memberships" "transmission_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_transmission_group.id

    members  = var.dev_transmission_users
}

resource "keycloak_group_memberships" "vault_admin_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_vault_admin_group.id

    members  = var.dev_vault_admin_users
}

resource "keycloak_group_memberships" "vault_ssh_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_vault_ssh_group.id

    members  = var.dev_vault_ssh_users
}

resource "keycloak_group_memberships" "vault_aws_admin_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_vault_aws_admin_group.id

    members  = var.dev_vault_aws_admin_users
}

resource "keycloak_group_memberships" "jenkins_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_jenkins_group.id

    members  = var.dev_jenkins_users
}

resource "keycloak_group_memberships" "traefik_group_members" {
    realm_id = keycloak_realm.dev_realm.id
    group_id = keycloak_group.dev_traefik_group.id

    members  = var.dev_traefik_users
}
