resource "keycloak_openid_client" "dev_couchpotato_client" {
    realm_id            = keycloak_realm.dev_realm.id
    client_id           = var.dev_couchpotato_client_name
    name                = var.dev_couchpotato_client_name
    enabled             = true

    standard_flow_enabled = true

    access_type         = "CONFIDENTIAL"
    valid_redirect_uris = [
        var.dev_couchpotato_cient_redirect_uri
    ]
}

resource "keycloak_role" "couchpotato_admin_realm_role" {
  realm_id    = keycloak_realm.dev_realm.id
  name        = var.dev_couchpotato_role_name
  description = var.dev_couchpotato_role_description
}

resource "keycloak_openid_user_realm_role_protocol_mapper" "couchpotato_realm_role_mapper" {
  realm_id  = keycloak_realm.dev_realm.id
  client_id = keycloak_openid_client.dev_couchpotato_client.id
  name      = "user-realm-role-mapper"
  claim_name = "groups"
}
