resource "keycloak_openid_client" "dev_jenkins_client" {
    realm_id            = keycloak_realm.dev_realm.id
    client_id           = var.dev_jenkins_client_name
    name                = var.dev_jenkins_client_name
    enabled             = true

    standard_flow_enabled = true

    access_type         = "PUBLIC"
    valid_redirect_uris = [
        var.dev_jenkins_cient_redirect_uri
    ]
}

resource "keycloak_role" "jenkins_admin_realm_role" {
  realm_id    = keycloak_realm.dev_realm.id
  name        = var.dev_jenkins_role_name
  description = var.dev_jenkins_role_description
}

resource "keycloak_openid_user_realm_role_protocol_mapper" "jenkins_realm_role_mapper" {
  realm_id  = keycloak_realm.dev_realm.id
  client_id = keycloak_openid_client.dev_jenkins_client.id
  name      = "user-realm-role-mapper"
  claim_name = "groups"
}
