resource "keycloak_openid_client" "dev_sickgear_client" {
    realm_id            = keycloak_realm.dev_realm.id
    client_id           = var.dev_sickgear_client_name
    name                = var.dev_sickgear_client_name
    enabled             = true

    standard_flow_enabled = true

    access_type         = "CONFIDENTIAL"
    valid_redirect_uris = [
        var.dev_sickgear_cient_redirect_uri
    ]
}

resource "keycloak_role" "sickgear_admin_realm_role" {
  realm_id    = keycloak_realm.dev_realm.id
  name        = var.dev_sickgear_role_name
  description = var.dev_sickgear_role_description
}

resource "keycloak_openid_user_realm_role_protocol_mapper" "sickgear_user_realm_role_mapper" {
    realm_id    = keycloak_realm.dev_realm.id
    client_id   = keycloak_openid_client.dev_sickgear_client.id
    name        = "groups"
    claim_name  = "groups"
    add_to_userinfo = true
    add_to_access_token = true
    add_to_id_token = true
    multivalued = true
}
