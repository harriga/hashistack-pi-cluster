resource "keycloak_group" "dev_sickbeard_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_sickbeard_group_name
}

resource "keycloak_group_roles" "group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_sickbeard_group.id

  role_ids = [
    keycloak_role.sickbeard_admin_realm_role.id,
  ]
}


resource "keycloak_group" "dev_couchpotato_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_couchpotato_group_name
}

resource "keycloak_group_roles" "couchpotato_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_couchpotato_group.id

  role_ids = [
    keycloak_role.couchpotato_admin_realm_role.id,
  ]
}


resource "keycloak_group" "dev_grafana_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_grafana_group_name
}

resource "keycloak_group_roles" "grafana_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_grafana_group.id

  role_ids = [
    keycloak_role.grafana_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_homebridge_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_homebridge_group_name
}

resource "keycloak_group_roles" "homebridge_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_homebridge_group.id

  role_ids = [
    keycloak_role.homebridge_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_sabnzb_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_sabnzb_group_name
}

resource "keycloak_group_roles" "sabnzb_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_sabnzb_group.id

  role_ids = [
    keycloak_role.sabnzb_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_transmission_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_transmission_group_name
}

resource "keycloak_group_roles" "transmission_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_transmission_group.id

  role_ids = [
    keycloak_role.transmission_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_vault_admin_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_vault_admin_group_name
}

resource "keycloak_group_roles" "vault_admin_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_vault_admin_group.id

  role_ids = [
    keycloak_role.vault_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_vault_ssh_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_vault_ssh_group_name
}

resource "keycloak_group_roles" "vault_ssh_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_vault_ssh_group.id

  role_ids = [
    keycloak_role.vault_ssh_realm_role.id,
  ]
}

resource "keycloak_group" "dev_vault_aws_admin_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_vault_aws_admin_group_name
}

resource "keycloak_group_roles" "vault_aws_admin_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_vault_aws_admin_group.id

  role_ids = [
    keycloak_role.vault_aws_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_jenkins_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_jenkins_group_name
}

resource "keycloak_group_roles" "jenkins_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_jenkins_group.id

  role_ids = [
    keycloak_role.jenkins_admin_realm_role.id,
  ]
}

resource "keycloak_group" "dev_traefik_group" {
  realm_id = keycloak_realm.dev_realm.id
  name     = var.dev_traefik_group_name
}

resource "keycloak_group_roles" "traefik_group_roles" {
  realm_id = keycloak_realm.dev_realm.id
  group_id = keycloak_group.dev_traefik_group.id

  role_ids = [
    keycloak_role.traefik_admin_realm_role.id,
  ]
}
