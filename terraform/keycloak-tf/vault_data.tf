provider "vault" {
  address = "http://active.vault.service.consul:8200"
}

data "vault_generic_secret" "keycloak_master_realm_secrets" {
  path = "kv/keycloak/realms/master"
}
