resource "vault_generic_secret" "sickbeard_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/sickbeard"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_sickbeard_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "sickgear_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/sickgear"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_sickgear_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "sabnzb_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/sabnzb"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_sabnzb_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "homebridge_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/homebridge"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_homebridge_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "couchpotato_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/couchpotato"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_couchpotato_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "traefik_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/traefik"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_traefik_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "transmission_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/transmission"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_transmission_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "grafana_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/grafana"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_grafana_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "jenkins_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/jenkins"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_jenkins_client.client_secret}"
}
EOT
}

resource "vault_generic_secret" "vault_openid_client" {
  path = "kv/keycloak/realms/${lower(var.dev_realm_name)}/vault"

  data_json = <<EOT
{
  "client_secret":   "${keycloak_openid_client.dev_vault_client.client_secret}"
}
EOT
}
