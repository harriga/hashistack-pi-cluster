resource "keycloak_realm" "dev_realm" {
  realm   = var.dev_realm_name
  enabled = true

  registration_allowed = false
  remember_me = false

  security_defenses {
    brute_force_detection {
      max_login_failures = 5
      wait_increment_seconds = 300
    }
  }
  login_theme = "govuk"
  email_theme = "govuk"

  display_name = var.dev_realm_display_name
  display_name_html = "<b>${var.dev_realm_display_name}</b>"
}
