terraform {
  required_providers {
    keycloak = {
      source  = "mrparkers/keycloak"
    }
  }
}


provider "keycloak" {
    client_id     = "terraform"
    client_secret = data.vault_generic_secret.keycloak_master_realm_secrets.data["client_secret"]
    url           = "https://keycloak.phiji.com"
    client_timeout = 1500
}
