variable dev_realm_name {
  type = string
  default = "Dev"
}

variable dev_realm_display_name {
  type = string
  default = "Dev Services"
}

variable dev_sickbeard_client_name {
  type = string
  default = "dev-sickbeard-client-oidc"
}

variable dev_sickbeard_cient_redirect_uri {
  type = string
  default = "https://sickbeard.phiji.com/*"
}

variable dev_sickbeard_role_description {
  type = string
  default = "Role for access to Sickbeard"
}

variable dev_sickbeard_role_name {
  type = string
  default = "dev-sickbeard-admin-role"
}


variable dev_sickbeard_group_name {
  type = string
  default = "dev-sickbeard-group"
}

variable dev_sickbeard_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_sickgear_client_name {
  type = string
  default = "dev-sickgear-client-oidc"
}

variable dev_sickgear_cient_redirect_uri {
  type = string
  default = "https://sickgear.phiji.com/*"
}

variable dev_sickgear_role_description {
  type = string
  default = "Role for access to Sickbeard"
}

variable dev_sickgear_role_name {
  type = string
  default = "dev-sickgear-admin-role"
}


variable dev_sickgear_group_name {
  type = string
  default = "dev-sickgear-group"
}

variable dev_sickgear_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_couchpotato_client_name {
  type = string
  default = "dev-couchpotato-client-oidc"
}

variable dev_couchpotato_cient_redirect_uri {
  type = string
  default = "https://couchpotato.phiji.com/*"
}

variable dev_couchpotato_role_description {
  type = string
  default = "Role for access to Couchpotato"
}

variable dev_couchpotato_role_name {
  type = string
  default = "dev-couchpotato-admin-role"
}


variable dev_couchpotato_group_name {
  type = string
  default = "dev-couchpotato-group"
}

variable dev_couchpotato_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_homebridge_client_name {
  type = string
  default = "dev-homebridge-client-oidc"
}

variable dev_homebridge_cient_redirect_uri {
  type = string
  default = "https://homebridge.phiji.com/*"
}

variable dev_homebridge_role_description {
  type = string
  default = "Role for access to Homebridge"
}

variable dev_homebridge_role_name {
  type = string
  default = "dev-homebridge-admin-role"
}


variable dev_homebridge_group_name {
  type = string
  default = "dev-homebridge-group"
}

variable dev_homebridge_users {
  type = list
  default = [
      "gav"
  ]
}
variable dev_grafana_client_name {
  type = string
  default = "dev-grafana-client-oidc"
}

variable dev_grafana_cient_redirect_uri {
  type = string
  default = "https://grafana.phiji.com/*"
}

variable dev_grafana_role_description {
  type = string
  default = "Role for access to Grafana"
}

variable dev_grafana_role_name {
  type = string
  default = "dev-grafana-admin-role"
}


variable dev_grafana_group_name {
  type = string
  default = "dev-grafana-group"
}

variable dev_grafana_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_sabnzb_client_name {
  type = string
  default = "dev-sabnzb-client-oidc"
}

variable dev_sabnzb_cient_redirect_uri {
  type = string
  default = "https://sabnzb.phiji.com/*"
}

variable dev_sabnzb_role_description {
  type = string
  default = "Role for access to SABnzb"
}

variable dev_sabnzb_role_name {
  type = string
  default = "dev-sabnzb-admin-role"
}


variable dev_sabnzb_group_name {
  type = string
  default = "dev-sabnzb-group"
}

variable dev_sabnzb_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_transmission_client_name {
  type = string
  default = "dev-transmission-client-oidc"
}

variable dev_transmission_cient_redirect_uri {
  type = string
  default = "https://transmission.phiji.com/*"
}

variable dev_transmission_role_description {
  type = string
  default = "Role for access to Transmission"
}

variable dev_transmission_role_name {
  type = string
  default = "dev-transmission-admin-role"
}


variable dev_transmission_group_name {
  type = string
  default = "dev-transmission-group"
}

variable dev_transmission_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_jenkins_client_name {
  type = string
  default = "jenkins"
}

variable dev_jenkins_cient_redirect_uri {
  type = string
  default = "https://jenkins.phiji.com/*"
}

variable dev_jenkins_role_description {
  type = string
  default = "Role for access to Jenkins"
}

variable dev_jenkins_role_name {
  type = string
  default = "jenkins_admin"
}


variable dev_jenkins_group_name {
  type = string
  default = "jenkins_admin"
}

variable dev_jenkins_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_traefik_client_name {
  type = string
  default = "dev-traefik-client-oidc"
}

variable dev_traefik_cient_redirect_uri {
  type = string
  default = "https://traefik.phiji.com/*"
}

variable dev_traefik_role_description {
  type = string
  default = "Role for access to Traefik"
}

variable dev_traefik_role_name {
  type = string
  default = "dev-traefik-admin-role"
}


variable dev_traefik_group_name {
  type = string
  default = "dev-traefik-group"
}

variable dev_traefik_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_vault_client_name {
  type = string
  default = "dev-vault-client-oidc"
}

variable dev_vault_cient_redirect_uri {
  type = list
  default = [
    "http://active.vault.service.consul:8200/ui/vault/auth/oidc/oidc/callback",
    "http://active.vault.service.consul:8250/oidc/callback",
    "http://localhost:8250/oidc/callback"
  ]
}

variable dev_vault_admin_role_description {
  type = string
  default = "Admin role for access to Vault"
}

variable dev_vault_admin_role_name {
  type = string
  default = "dev-vault-admin-role"
}

variable dev_vault_aws_admin_role_description {
  type = string
  default = "Admin role for access to AWS via Vault"
}

variable dev_vault_aws_admin_role_name {
  type = string
  default = "dev-aws-admin-role"
}

variable dev_vault_aws_admin_group_name {
  type = string
  default = "dev-vault-aws-admin-group"
}

variable dev_vault_ssh_role_description {
  type = string
  default = "SSH role for access to nodes"
}

variable dev_vault_ssh_role_name {
  type = string
  default = "dev-vault-ssh-role"
}

variable dev_vault_admin_group_name {
  type = string
  default = "dev-vault-admin-group"
}

variable dev_vault_ssh_group_name {
  type = string
  default = "dev-vault-ssh-group"
}

variable dev_vault_admin_users {
  type = list
  default = [
      "gav"
  ]
}

variable dev_vault_ssh_users {
  type = list
  default = [
    "gav"
  ]
}

variable dev_vault_aws_admin_users {
  type = list
  default = [
    "gav"
  ]
}


variable dev_users {
  type = map
  default = {
      "gav"  = "gavin@phiji.com"
  }
}
