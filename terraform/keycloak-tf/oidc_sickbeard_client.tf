resource "keycloak_openid_client" "dev_sickbeard_client" {
    realm_id            = keycloak_realm.dev_realm.id
    client_id           = var.dev_sickbeard_client_name
    name                = var.dev_sickbeard_client_name
    enabled             = true

    standard_flow_enabled = true

    access_type         = "CONFIDENTIAL"
    valid_redirect_uris = [
        var.dev_sickbeard_cient_redirect_uri
    ]
}

resource "keycloak_role" "sickbeard_admin_realm_role" {
  realm_id    = keycloak_realm.dev_realm.id
  name        = var.dev_sickbeard_role_name
  description = var.dev_sickbeard_role_description
}

resource "keycloak_openid_user_realm_role_protocol_mapper" "sickbeard_user_realm_role_mapper" {
    realm_id    = keycloak_realm.dev_realm.id
    client_id   = keycloak_openid_client.dev_sickbeard_client.id
    name        = "groups"
    claim_name  = "groups"
    add_to_userinfo = true
    add_to_access_token = true
    add_to_id_token = true
    multivalued = true
}
