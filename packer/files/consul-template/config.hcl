vault {
  address = "http://active.vault.service.consul:8200"
  token = "NOMAD_VAULT_TOKEN"
  renew_token = false
}

syslog {
  enabled = true
  facility = "LOCAL5"
}

template {
  contents="{{ with secret \"pki/issue/service.consul\" \"common_name=nomad.service.consul\" \"alt_names=localhost,server.global.nomad\" \"ip_sans=127.0.0.1\" }}{{ .Data.certificate }}{{ end }}"
  destination="/nomad/conf.d/tls/nomad.service.consul.crt"
  command = "systemctl reload nomad"
}

template {
  contents="{{ with secret \"pki/issue/service.consul\" \"common_name=nomad.service.consul\" \"alt_names=localhost,server.global.nomad\" \"ip_sans=127.0.0.1\"}}{{ .Data.issuing_ca }}{{ end }}"
  destination="/nomad/conf.d/tls/nomad.service.consul.ca"
  command = "systemctl reload nomad"
}

template {
  contents="{{ with secret \"pki/issue/service.consul\" \"common_name=nomad.service.consul\" \"alt_names=localhost,server.global.nomad\" \"ip_sans=127.0.0.1\"}}{{ .Data.issuing_ca }}{{ end }}"
  destination="/nfs/nomad/jenkins/ca.crt"
}

template {
  contents="{{ with secret \"pki/issue/service.consul\" \"common_name=nomad.service.consul\" \"alt_names=localhost,server.global.nomad\" \"ip_sans=127.0.0.1\"}}{{ .Data.private_key }}{{ end }}"
  destination="/nomad/conf.d/tls/nomad.service.consul.key"
  command = "systemctl reload nomad"
}

template {
  contents="{{ with secret \"ssh/config/ca\"}}{{ .Data.public_key }}{{ end }}"
  destination="/etc/ssh/trusted-user-ca-keys.pem"
}
