#!/bin/sh

HOSTNAME=$(ifconfig wlan0| grep "ether" | cut -d" " -f10 | sed s/://g)

echo $HOSTNAME > /etc/hostname

hostname $HOSTNAME
