region = "global"
datacenter = "DC0"

advertise {
    http = "{{GetInterfaceIP \"wlan0\"}}"
    rpc = "{{GetInterfaceIP \"wlan0\"}}"
    serf = "{{GetInterfaceIP \"wlan0\"}}"
}

consul {
    # The address to the Consul agent.
    address = "localhost:8500"
    # The service name to register the server and client with Consul.
    server_service_name = "nomad"
    client_service_name = "nomad-clients"

    # Enables automatically registering the services.
    auto_advertise = true

    # Enabling the server and client to bootstrap using Consul.
    server_auto_join = true
    client_auto_join = true
}


vault {
    # Enable vault integration
    enabled     = true
    address     = "http://vault.service.consul:8200"
    token       = "NOMAD_VAULT_TOKEN"
    create_from_role = "nomad-cluster"
}

data_dir = "/nomad/data"

log_level = "INFO"
enable_syslog = true
