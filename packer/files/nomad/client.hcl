client {
  enabled = true
  meta {
  }
  host_volume "jenkins_volume" {
    path = "/nfs/nomad/jenkins"
    read_only = false
  }
  host_volume "keycloak_mysql_volume" {
    path = "/nfs/nomad/keycloak_mysql"
    read_only = false
  }
  host_volume "boundary_postgres_volume" {
    path = "/nfs/nomad/boundary_postgres"
    read_only = false
  }
  host_volume "pact_postgres_volume" {
    path = "/nfs/nomad/pact_postgres"
    read_only = false
  }
  host_volume "acme_volume" {
    path = "/nfs/nomad/acme"
    read_only = false
  }
  host_volume "util_mysql" {
    path = "/nfs/nomad/util_mysql"
    read_only = false
  }
  host_volume "grafana_conf" {
    path = "/nfs/nomad/grafana_conf"
    read_only = false
  }
  host_volume "grafana_data" {
    path = "/nfs/nomad/grafana_data"
    read_only = false
  }
}

plugin "docker" {
  config {
    allow_privileged = true
    allow_caps = [ "ALL" ]
    volumes {
      enabled      = true
    }
  }
}
