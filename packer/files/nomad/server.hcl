server {
  enabled = true
  raft_protocol = 3
  bootstrap_expect = 3
}

acl {
  enabled = true
}

tls {
  http = true
  rpc  = true

  ca_file   = "/nomad/conf.d/tls/nomad.service.consul.ca"
  cert_file = "/nomad/conf.d/tls/nomad.service.consul.crt"
  key_file  = "/nomad/conf.d/tls/nomad.service.consul.key"

  verify_server_hostname = true
  verify_https_client    = false
}
