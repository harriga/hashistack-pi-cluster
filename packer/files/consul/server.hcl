server = true
datacenter = "DC0"
data_dir = "/consul/data"
ui = true
bind_addr = "{{GetInterfaceIP \"wlan0\"}}"
client_addr = "0.0.0.0"

bootstrap_expect = 5

encrypt = "CONSUL_GOSSIP_KEY"

retry_join = [
    "PI1",
    "PI2",
    "PI3",
    "PI4",
    "PI5"
]
