{
  "variables": {
    "wifi_name": "",
    "wifi_password": "",
    "home": "{{env `HOME`}}"
  },
  "builders": [
      {
      "type": "arm-image",
      "iso_url": "https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-{{user `raspbian_image_date`}}/{{user `raspbian_image_version`}}-raspbian-buster-lite.zip",
      "iso_checksum": "sha256:{{user `raspbian_iso_checksum`}}",
      "target_image_size": 4294967296
    }
  ],  
  "provisioners": [
    {
      "type": "shell",
      "inline": ["touch /boot/ssh"]
    },
    {
  "type": "shell",
  "inline": [
    "echo 'country=GB\n\n' >> /etc/wpa_supplicant/wpa_supplicant.conf",
    "echo 'network={' >> /etc/wpa_supplicant/wpa_supplicant.conf",
    "echo '    ssid=\"{{user `wifi_name`}}\"' >> /etc/wpa_supplicant/wpa_supplicant.conf",
    "echo '    psk=\"{{user `wifi_password`}}\"' >> /etc/wpa_supplicant/wpa_supplicant.conf",
    "echo '}' >> /etc/wpa_supplicant/wpa_supplicant.conf",
    "mkdir /home/pi/.ssh && chown pi:pi /home/pi/.ssh && chmod 700 /home/pi/.ssh",
    "apt-get update",
    "curl -fsSL https://get.docker.com -o /tmp/get-docker.sh",
    "sh /tmp/get-docker.sh",
    "apt-get upgrade -y",
    "apt-get install wget -y",
    "apt-get install unzip -y",
    "apt-get install bind9utils -y",
    "apt-get install unbound -y",
    "apt-get install ntp -y",
    "cp /etc/wpa_supplicant/wpa_supplicant.conf /boot/",
    "echo -n \" cgroup_memory=1 swapaccount=1\" >> /boot/cmdline.txt",
    "mkdir /etc/docker",
    "systemctl enable ntp"
    ]
    },
    {
      "type": "file",
      "source": "{{user `home`}}/.ssh/id_rsa.pub",
      "destination": "/home/pi/.ssh/authorized_keys"
    },
    {
      "type": "file",
      "source": "files/docker/docker.config",
      "destination": "/etc/docker/daemon.json"
    },
    { "type": "shell",
      "inline": [
        "sed -i s/PI1/{{user `raspberry_pi_1_ip`}}/ /etc/docker/daemon.json",
        "sed -i s/PI2/{{user `raspberry_pi_2_ip`}}/ /etc/docker/daemon.json",
        "sed -i s/PI3/{{user `raspberry_pi_3_ip`}}/ /etc/docker/daemon.json",
        "sed -i s/PI4/{{user `raspberry_pi_4_ip`}}/ /etc/docker/daemon.json",
        "sed -i s/PI5/{{user `raspberry_pi_5_ip`}}/ /etc/docker/daemon.json"
      ]
    },
    {
      "type": "shell",
      "inline": [
	      "chown pi:pi /home/pi/.ssh/authorized_keys",
        "sed '/PasswordAuthentication/d' -i /etc/ssh/sshd_config",
        "echo  >> /etc/ssh/sshd_config",
        "echo 'PasswordAuthentication no' >> /etc/ssh/sshd_config",
	      "mkdir -p /nomad/data /nomad/jobs /nomad/conf.d /nomad/conf.d/tls/client /nomad/conf.d/tls/server /consul/data /consul/conf.d /vault/conf.d /rename /consul-template/conf.d"
      ]
    },
    {
      "type": "file",
      "source": "files/nomad/nomad.service",
      "destination": "/etc/systemd/system/nomad.service"
    },
    {
      "type": "file",
      "source": "files/nomad/server.hcl",
      "destination": "/nomad/conf.d/server.hcl"
    },
    {
      "type": "file",
      "source": "files/nomad/client.hcl",
      "destination": "/nomad/conf.d/client.hcl"
    },
    {
      "type": "file",
      "source": "files/nomad/base.hcl",
      "destination": "/nomad/conf.d/base.hcl"
    },
    {
      "type": "file",
      "source": "files/consul-template/config.hcl",
      "destination": "/consul-template/conf.d/config.hcl"
    },
    { "type": "shell",
      "inline": [
        "sed -i s/NOMAD_VAULT_TOKEN/{{user `nomad_vault_token`}}/ /nomad/conf.d/base.hcl",
	      "sed -i s/NOMAD_VAULT_TOKEN/{{user `nomad_vault_token`}}/ /consul-template/conf.d/config.hcl"
      ]
    },
    {
      "type": "shell",
      "inline": [
	      "mkdir /installers",
	      "cd /installers && wget https://releases.hashicorp.com/nomad/{{user `nomad_version`}}/nomad_{{user `nomad_version`}}_linux_arm.zip",
	      "unzip /installers/nomad_{{user `nomad_version`}}_linux_arm.zip",
	      "mv /installers/nomad /usr/bin/nomad",
	      "useradd nomad -s /bin/false -d /nomad/conf.d -G docker",
	      "chown -R nomad:nomad /nomad",
	      "systemctl enable nomad"
      ]
    },
    {
      "type": "file",
      "source": "files/consul-template/consul-template.service",
      "destination": "/etc/systemd/system/consul-template.service"
    },
    {
      "type": "shell",
      "inline": [
        "cd /installers && wget https://releases.hashicorp.com/consul-template/{{user `consul_template_version`}}/consul-template_{{user `consul_template_version`}}_linux_arm.zip",
        "unzip /installers/consul-template_{{user `consul_template_version`}}_linux_arm.zip",
        "mv /installers/consul-template /usr/bin/consul-template",
        "systemctl enable consul-template"
      ]
    },
    {
      "type": "file", 
      "source": "files/consul/consul.service",
      "destination": "/etc/systemd/system/consul.service"
    },
    {
      "type": "file",
      "source": "files/consul/server.hcl",
      "destination": "/consul/conf.d/server.hcl"
    },
    { "type": "shell", 
      "inline": [
        "sed -i s/PI1/{{user `raspberry_pi_1_ip`}}/ /consul/conf.d/server.hcl",
        "sed -i s/PI2/{{user `raspberry_pi_2_ip`}}/ /consul/conf.d/server.hcl",
	      "sed -i s/PI3/{{user `raspberry_pi_3_ip`}}/ /consul/conf.d/server.hcl",
	      "sed -i s/PI4/{{user `raspberry_pi_4_ip`}}/ /consul/conf.d/server.hcl",
	      "sed -i s/PI5/{{user `raspberry_pi_5_ip`}}/ /consul/conf.d/server.hcl",
	      "sed -i s/CONSUL_GOSSIP_KEY/{{user `consul_key`}}/ /consul/conf.d/server.hcl"
      ]
    },
    {
      "type": "shell",
      "inline": [
        "cd /installers && wget https://releases.hashicorp.com/terraform/{{user `terraform_version`}}/terraform_{{user `terraform_version`}}_linux_arm.zip",
	      "unzip terraform_{{user `terraform_version`}}_linux_arm.zip",
        "mv /installers/terraform /usr/bin/terraform"
      ]
    },
    {
      "type": "shell",
      "inline": [
        "cd /installers && wget https://releases.hashicorp.com/consul/{{user `consul_version`}}/consul_{{user `consul_version`}}_linux_armhfv6.zip",
        "unzip /installers/consul_{{user `consul_version`}}_linux_armhfv6.zip",
        "mv /installers/consul /usr/bin/consul",
	      "useradd consul -s /bin/false -d /consul/conf.d",
	      "chown -R consul:consul /consul",
	      "systemctl enable consul"
      ]
    },
    {
      "type": "file",
      "source": "files/vault/server.hcl",
      "destination": "/vault/conf.d/server.hcl"
    },
    {
      "type": "file",
      "source": "files/vault/vault.service",
      "destination": "/etc/systemd/system/vault.service"
    },
    {
      "type": "shell",
      "inline": [
        "cd /installers && wget https://releases.hashicorp.com/vault/{{user `vault_version`}}/vault_{{user `vault_version`}}_linux_arm.zip",
        "unzip /installers/vault_{{user `vault_version`}}_linux_arm.zip",
        "mv /installers/vault /usr/bin/vault",
        "useradd vault -s /bin/false -d /vault/conf.d",
        "chown -R vault:vault /vault",
        "systemctl enable vault"
      ]
    },
    {
      "type": "file",
      "source": "files/rename/rename.sh",
      "destination": "/rename/rename.sh"
    },
    {
      "type": "file",
      "source": "files/rename/rename.service",
      "destination": "/etc/systemd/system/rename.service"
    },
    {
      "type": "shell",
      "inline": [
	      "chmod 755 /rename/rename.sh",
        "systemctl enable rename",
	      "mkdir -p /nfs/nomad",
	      "echo '{{user `nas_host_ip`}}:{{user `nas_host_share`}}  /nfs/nomad      nfs     vers=3,soft     0       1' >> /etc/fstab",
        "echo 'export VAULT_ADDR=http://127.0.0.1:8200' >> /etc/profile",
        "echo 'TrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pem' >> /etc/ssh/sshd_config"
      ]
    },
    {
      "type": "file",
      "source": "files/unbound/unbound.config",
      "destination": "/etc/unbound/unbound.conf.d/consul.conf"
    },
    {
      "type": "shell",
      "inline": [
        "systemctl enable unbound"
      ]
    }
  ]
}
