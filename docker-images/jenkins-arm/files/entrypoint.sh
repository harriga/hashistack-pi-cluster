#!/bin/sh

if [ -f "/var/jenkins_home/ca.crt" ]; then
    keytool -import -alias vault_root_ca -trustcacerts -file /var/jenkins_home/ca.crt -keystore /usr/lib/jvm/java-8-openjdk-armhf/jre/lib/security/cacerts -storepass changeit -noprompt
fi

/usr/bin/java -jar /opt/jenkins.war
