#!/bin/bash

function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

CURRENT_IP=`curl -sSL https://ifconfig.co/json | jq -r .ip`

echo "Current IP: ${CURRENT_IP}"

if ! valid_ip $CURRENT_IP; then
    echo "Couldn't get current IP: $CURRENT_IP"
    exit 1
fi

CURRENT_AWS_IP=`dig +short ${RESOURCE_RECORD}`

echo "Current AWS IP: ${CURRENT_AWS_IP}"

if ! valid_ip $CURRENT_AWS_IP; then
    echo "Couldn't get current AWS IP: $CURRENT_AWS_IP"
    exit 1
fi

if [ "$CURRENT_IP" == "$CURRENT_AWS_IP" ]
    then
    echo "IP is up to date"
    exit 0
fi

TMPFILE=$(mktemp /tmp/temporary-file.XXXXXXXX)

cat > ${TMPFILE} << EOF
{
  "Comment":"Updated by ddns",
  "Changes":[
    {
      "Action":"UPSERT",
      "ResourceRecordSet":{
        "ResourceRecords":[
          {
            "Value":"$CURRENT_IP"
          }
        ],
        "Name":"$RESOURCE_RECORD",
        "Type":"$RESOURCE_TYPE",
        "TTL":$RESOURCE_TTL
      }
    }
  ]
}
EOF

aws route53 change-resource-record-sets --hosted-zone-id $AWS_HOSTED_ZONE_ID --change-batch file://"$TMPFILE"

rm $TMPFILE
